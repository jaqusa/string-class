#include <iostream>
#include <string>

using namespace std;

int countTimes(string phrase, string word) {
	int times = 0, start = 0;

	while ((start = phrase.find(word, start) )!= -1) {
		times++;
		start += word.length();
	}

	return times;
}

int main() {
	string phrase;
	string word;

	cout << "enter a phrase -> ";
	getline(cin, phrase);
	cout << "enter a word -> ";
	cin >> word;

	cout << countTimes(phrase, word) << endl;


}