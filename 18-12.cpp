#include <iostream>
#include <string>
#include <fstream>
#include <time.h>

using namespace std;

string randomWord(string repository) {
	string word[20];
	ifstream file(repository);
	srand(time(NULL));
	int position = (rand() % 20);

	for (int i = 0; i < 20; i++) {
		file >> word[i];
	}


	return word[position];
}

string searchLetter(string word, string letter, string chain) {
	int start = 0;
	int found;
	while ((start = word.find(letter, start)) != -1 && letter != "X") {
		chain[start] = word[start];
		start++;
	}
	if (letter != "X") {
		return chain;
	}

	found = chain.find(letter);
	if (found == -1 && letter == "X") {
		return "1";
	}

	return "0";
}

void showBody(int attempt, string& body) {
	string head = "O";
	string left = "/";
	string trunk = "|";
	string right = "\\";

	switch (attempt) {
	case 1:
		body = " " + head + "\n";
		break;
	case 2:
		body += left;
		break;
	case 3:
		body += trunk;
		break;
	case 4:
		body += right + "\n";
		break;
	case 5:
		body += " " + trunk + "\n";
		break;
	case 6:
		body += left;
		break;
	case 7:
		body += " " + right + "\n";
		break;
	}
	cout << endl;
	cout << body << endl;
	cout << endl;

}
bool endGame(string word, string chain) {
	string a = searchLetter(word, "X", chain).c_str();
	bool b = stoi(a);
	if (b) {
		cout << "Felicidades!!! Adivino la palabra " << word << "." << endl;
		return  true;
	}
	return false;

}

void showWord(string chain, string& letter, string& letters, bool  end) {
	cout << "Adivine la palabra: " << chain << endl;

	if (!end) {
		cout << "ingrese una letra: ";
		getline(cin, letter);
		if (letters.length() == 0) {
			letters = letter;
		}
		else {
			letters += "," + letter;
		}

		cout << "Intentos realizados: " << letters << endl;
	}

}

int main() {
	bool end = false;

	string  letter = "", wordsAttemps = "", body = "";
	int sum = 0;
	int attemps = 0;

	string word = randomWord("wordsList.txt");
	string chain(word.length(), 'X');

	while (attemps < 7) {
		end = endGame(word, chain);
		showWord(chain, letter, wordsAttemps, end);
		if (end) { break; }
		string newChain = searchLetter(word, letter, chain);

		cout << "es correcto: ";
		if (chain.compare(newChain) == 0) {
			cout << "no" << endl;
			showBody(++attemps, body);
		}
		else {
			cout << "si" << endl;
		}

		chain = newChain;
	}


}