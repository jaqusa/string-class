#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string generateNumbers(int range) {
	string chain;
	for (int i = 0; i < range; i++) {
		chain += to_string((rand() % 1000) + 1) + "\n";
	}

	return chain;
}

int main() {
	int n;
	ofstream file("numeros.txt");
	do {
		cout << "enter a number between 1 - 10 -> ";
		cin >> n;
	} while (n > 10 || n == 0);


	file << generateNumbers(n);
	cout << "done" << endl;


}